package tea.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import tea.api.TeaDto;
import tea.domaine.Tea;
import tea.domaine.TeaRepository;

@Component
public class TeaController {
	
	private final TeaRepository repo;
	private final TeaConverter converter;
	
	public TeaController(TeaRepository repo, TeaConverter converter) {
		this.repo = repo;
		this.converter = converter;
	}

	public void create(TeaDto teaDto) {
		Tea tea = converter.toTea(teaDto);
		repo.save(tea);
	}

	public List<TeaDto> get() {
		return repo.getAll().stream()
				.map(tea -> converter.fromTea(tea))
				.collect(Collectors.toList());
	}

	public TeaDto get(Long id) {
		return converter.fromTea(repo.get(id));
	}

	public List<TeaDto> get(String teaName, String teaCategory) {
		return repo.search(teaName, teaCategory).stream()
				.map(tea -> converter.fromTea(tea))
				.collect(Collectors.toList());
	}

	public void update(TeaDto tea) {
		repo.save(converter.toTea(tea));
	}

	public void delete(Long id) {
		repo.delete(id);
	}
}
